#include <stdio.h>

//Sestrojte konecny automat nad abecedou {A,B}, ktery prijima prave slova, ktera obsahuji sekvenci ABBA a zaroven maji sudy pocet znaku.
//Nasledne tento automat naprogramujete v textovem rezimu.
//Cili uzivatel postupne zadava znaky, po jednom, a automat po kazdem kroku vypise,
//ve kterem je stavu a jestli je tento stav prijimajici. Pokud zada X, program se ukonci.

int main() {
    char table[10][2] = {
            {5, 9},
            {5, 6},
            {5, 7},
            {8, 9},
            {8, 8},
            {1, 2},
            {1, 3},
            {4, 0},
            {4, 4},
            {1, 0}
    };
    char charTable[256];
    charTable['A'] = 0;
    charTable['B'] = 1;
    char state = 0;
    int input = 0;
    printf("Zadejte A nebo B, pro ukonceni X.\n");
    while(1) {
        if(state == 4) {
            printf("Jste ve prijimajicim stavu %i\n", (int) state);
        } else {
            printf("Jste ve stavu  %i\n", (int) state);
        }
        input = fgetc(stdin);
        fgetc(stdin); //pozere odradkovani
        if(input != 'A' && input != 'B' && input != 'X' && input != EOF) {
            printf("Nespravny vstup!\n");
            continue;
        }
        if(input == 'X') {
            break;
        }
        state = table[state][charTable[input]];
    }
    printf("Konec\n");
    return 0;
}